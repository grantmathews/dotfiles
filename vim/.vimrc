set nocompatible
filetype plugin on
syntax on


set hidden

set formatoptions+=j

set wildmenu
set wildmode=longest:full,full

set laststatus=2
set backspace=2

set hlsearch

"; set mouse=a
set tabpagemax=50
colo ron

:nnoremap <F5> "=strftime("%F %T %Z")<CR>P
:inoremap <F5> <C-R>=strftime("%F %T %Z")<CR>

"; http://stackoverflow.com/a/2120168
let g:lasttab = 1
nmap <Leader>6 :exe "tabn ".g:lasttab<CR>
nmap <Leader>^ :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

nmap <Leader>s :syntax sync fromstart<CR>

";
"; mostly stolen from vimacs
";

inoremap <C-x><C-c> <C-o>:confirm qall<CR>
inoremap <C-x><C-w> <C-o>:write<Space>
noremap <C-x><C-u> <C-o>u

inoremap <C-u> <C-o>d0

cmap <C-a> <Home>
imap <C-a> <Home>
vmap <C-a> <Home>
omap <C-a> <Home>
cmap <C-e> <End>
imap <C-e> <End>
vmap <C-e> <End>
omap <C-e> <End>

inoremap <C-Up> <C-o>{
vnoremap <C-Up> {
onoremap <C-Up> {
inoremap <C-Down> <C-o>}
vnoremap <C-Down> }
onoremap <C-Down> }

";inoremap <C-t> <Left><C-o>x<C-o>p
inoremap <M-t> <Esc>dawbhpi
inoremap <C-x><C-t> <Up><C-o>dd<End><C-o>p<Down>

"; Avoid listing garbage in netrw
let g:netrw_list_hide= '.*\.sw[nop]$,.*\.py[co]$,^__pycache__/$'

"; Markdown and no C-space for vimwiki
let g:vimwiki_list = [{'path': $VWIKI_BASE . '/',
                      \ 'diary_rel_path': $VWIKI_DIARY . '/',
                      \ 'syntax': 'markdown', 'ext': '.md',
                      \ 'auto_tags': 1},
                      \ {'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md',
                      \ 'auto_tags': 1}
                      \ ]
map <Leader><Space> <Plug>VimwikiToggleListItem

"; Hack in an entry command
function! HackyDiaryEntry(...)
  let l:fn = strftime('%Y-%m-%d.md')
  if a:0
    let l:subdir = a:1 . '/'
  else
    let l:subdir = ''
  endif
  let l:conf = g:vimwiki_list[0]
  let l:dir = l:conf['path'] . l:conf['diary_rel_path'] . l:subdir
  silent exe '!mkdir -p ' . l:dir
  silent exe '!touch ' . l:dir . '.up'
  "; Since :tab doesn't work with :command, just open a tab if we're already
  "; in a named buffer:
  if bufname('%') != ''
    let l:cmd = 'tabedit ' . l:dir . l:fn
  else
    let l:cmd = 'edit ' . l:dir . l:fn
  endif
  execute  l:cmd
  execute 'lcd '.l:dir
endfunction
command -nargs=? Entry call HackyDiaryEntry(<f-args>)

"; Autocommit wiki stuff
augroup vimwikigithook
au!
au BufWritePost ~/vimwiki/* !git -C "%:h" add "%:t";git -C "%:h" commit -m "Auto commit of %:t."
augroup END

"; stop hiding URLs in vimwiki
let g:vimwiki_conceallevel = 0

"; Locally disable some plugins by telling them they're already loaded:
let g:loaded_logipat = 1

"; Highlight trailing space; adapted from:
"; http://vim.wikia.com/wiki/Highlight_unwanted_spaces
highlight TrailingWhitespace ctermbg=red guibg=red
match TrailingWhitespace /\s\+$/
autocmd BufWinEnter * match TrailingWhitespace /\s\+$/
autocmd InsertEnter * match TrailingWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match TrailingWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

"; Mostly stolen from:
"; http://vim.wikia.com/wiki/Move_cursor_by_display_lines_when_wrapping
noremap <silent> <Leader>w :call ToggleWrap()<CR>
function ToggleWrap()
  if &lbr
    echo "Wrap OFF"
    setlocal nolbr sbr=
    silent! nunmap <buffer> k
    silent! nunmap <buffer> j
    silent! nunmap <buffer> 0
    silent! nunmap <buffer> $
    silent! nunmap <buffer> ^
    silent! nunmap <buffer> I
    silent! nunmap <buffer> A
    silent! nunmap <buffer> <Up>
    silent! nunmap <buffer> <Down>
    silent! nunmap <buffer> <Home>
    silent! nunmap <buffer> <End>
    silent! iunmap <buffer> <Up>
    silent! iunmap <buffer> <Down>
    silent! iunmap <buffer> <Home>
    silent! iunmap <buffer> <End>
  else
    echo "Wrap ON"
    setlocal lbr sbr=+
    noremap  <buffer> <silent> k gk
    noremap  <buffer> <silent> j gj
    noremap  <buffer> <silent> 0 g0
    noremap  <buffer> <silent> $ g$
    noremap  <buffer> <silent> ^ g^
    noremap  <buffer> <silent> I g0i
    noremap  <buffer> <silent> A g$a
    noremap  <buffer> <silent> <Up>   gk
    noremap  <buffer> <silent> <Down> gj
    noremap  <buffer> <silent> <Home> g<Home>
    noremap  <buffer> <silent> <End>  g<End>
    inoremap <buffer> <silent> <Up>   <C-o>gk
    inoremap <buffer> <silent> <Down> <C-o>gj
    inoremap <buffer> <silent> <Home> <C-o>g<Home>
    inoremap <buffer> <silent> <End>  <C-o>g<End>
  endif
endfunction

" Transparent editing of gpg encrypted files.
" https://vim.fandom.com/wiki/Encryption
augroup encrypted
au!

" Avoid leaking info
autocmd BufReadPre *\[enc\]* set viminfo=
autocmd BufReadPre *\[enc\]* set noswapfile
autocmd BufReadPre *\[enc\]* set noshelltemp

autocmd BufReadPost *\[enc\]* :%!gpg --decrypt -q

" Encrypt to write, then undo to get back text
autocmd BufWritePre *\[enc\]* :%!gpg --default-recipient-self -ae
autocmd BufWritePost *\[enc\]* u

augroup END
