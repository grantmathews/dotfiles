# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !

#export PERLBREW_ROOT=${HOME}/perlbrew
#source ${PERLBREW_ROOT}/etc/bashrc
#source ~/perl5/perlbrew/etc/bashrc

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Source global definitions (for CentOS)
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Put your fun stuff here.

function tmux_helper() {
	if [ "${TERM#*256}" != "$TERM" ]; then
		# $TERM contains "256"; force 256-color tmux
		tmux -2 "$@"
	else
		tmux "$@"
	fi
}

# Vim entry config/helpers
export VWIKI_BASE="$HOME/vimwiki/personal"
export VWIKI_DIARY="diary"
function vimwiki() {
	mkdir -p $VWIKI_BASE
	pushd $VWIKI_BASE
	vim "${1:-index.md}"
	popd
}
function _vimwiki_complete() {
	COMPREPLY=()
	for path in $VWIKI_BASE/$2*{.md,.txt,/}
	do
		[ -e "$path" ] && COMPREPLY+=( "${path#$VWIKI_BASE/}" )
	done
}
complete -F _vimwiki_complete -o nospace vimwiki

function vent() {
	mkdir -p $VWIKI_BASE/$VWIKI_DIARY/$1
	pushd $VWIKI_BASE/$VWIKI_DIARY/$1
	vim "+Entry $1"
	popd
}
function _vent_complete() {
	COMPREPLY=()
	for path in $VWIKI_BASE/$VWIKI_DIARY/$2*/
	do
		COMPREPLY+=( $(basename $path) )
	done
}
complete -F _vent_complete vent

function entsync() {
	# Sync entries in three phases:
	# 1. Push local stuff to per-host branch, just in case we fuck up the local copy
	# 2. Try to integrate changes
	# 3. If we didn't fail, push the result
	VWGIT="git -C $VWIKI_BASE"
	$VWGIT push origin HEAD:$(hostname) && \
	$VWGIT pull origin master && \
	$VWGIT push origin HEAD:master
}

ddg() {
  IFS=+ w3m https://duckduckgo.com/html/?q="$*"
}

weather() {
  # TODO: figure out API instead of page scraping
  for wurl in \
'https://forecast.weather.gov/MapClick.php?CityName=Oakland&state=CA&site=MTR&lat=37.8091&lon=-122.27' \
'https://forecast.weather.gov/MapClick.php?CityName=San+Francisco&state=CA&site=MTR&lat=37.775&long=-122.418'
  do
    echo "$wurl" | sed -E -e 's/.*CityName=([^&]*).*/\1/' -e 's/\+/ /g'
    w3m -cols $(( $(tput cols) - 9 )) "$wurl" \
    | sed -nE '/Detailed Forecast/,/(Sun|Mon|Tues|Wednes|Thurs|Fri|Satur)day/p' \
    | sed -Ee '1,2d' -e '$d'  -e '/^To(day|night)$/{N; s/\n/: /}'
    echo
  done
}

PATH="$PATH:/usr/sbin:/sbin:$HOME/.local/bin"
HISTCONTROL=ignoreboth
HISTSIZE=2000
shopt -s histappend

export EDITOR='/usr/bin/vim'
export LESS='-X -R -M --shift 5'
export SCIPY_PIL_IMAGE_VIEWER=sxiv
#export LIBVA_DRIVER_NAME=vdpau

# PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
alias less='less -i'
alias bb_mplayer='mplayer -af equalizer=3:2:1:0:0:0:0:0:0:0'
alias tmux=tmux_helper

# wayland
export XDG_RUNTIME_DIR=/tmp/.runtime-${USER}
if [ ! -d "${XDG_RUNTIME_DIR}" ]; then
	mkdir -p "${XDG_RUNTIME_DIR}"
	chmod 0700 "${XDG_RUNTIME_DIR}"
fi

export GPG_TTY=$(tty)
