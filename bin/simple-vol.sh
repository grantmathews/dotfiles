#!/bin/sh

case $1 in
up)
  MARGS="-i 3 -u"
  ;;
down)
  MARGS="-d 3"
  ;;
toggle)
  MARGS="-t"
  ;;
*)
  echo -e "\nUsage: $0 [up|down|toggle]\n"
  exit 1
  ;;
esac

pamixer $MARGS || echo "ERR"
VOL=$(pamixer --get-volume)

if pamixer --get-mute > /dev/null ; then
  echo "Mute ($VOL%)"
else
  [ "$1" = "up" ] && paplay --volume 40000 /usr/share/sounds/freedesktop/stereo/bell.oga &
  echo "$VOL%"
fi
