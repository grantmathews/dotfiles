# Stolen from /etc/acpi/actions/powerbtn.sh, which stole it from Debian

# Used to setup X server access for root user in scripts, e.g.
#
#   [ -z "$DISPLAY" ] && getXconsole
#

# getXuser gets the X user belonging to the display in $displaynum.
# If you want the foreground X user, use getXconsole!
# Input:
#   displaynum - X display number
# Output: 
#   XUSER - the name of the user
#   XAUTHORITY - full pathname of the user's .Xauthority file
getXuser() {
        user=`pinky -fw | awk '{ if ($2 == ":'$displaynum'" || $(NF) == ":'$displaynum'" ) { print $1; exit; } }'`
        if [ x"$user" = x"" ]; then
                startx=`pgrep -n startx`
                if [ x"$startx" != x"" ]; then
                        user=`ps -o user --no-headers $startx`
                fi
        fi
        if [ x"$user" != x"" ]; then
                userhome=`getent passwd $user | cut -d: -f6`
                export XAUTHORITY=$userhome/.Xauthority
        else
                export XAUTHORITY=""
        fi
        export XUSER=$user
}

# Gets the X display number for the active virtual terminal.
# Output:
#   DISPLAY - the X display number
#   See getXuser()'s output.
getXconsole() {
        console=`fgconsole`;
        displaynum=`ps t tty$console | sed -n -re 's,.*/X .*:([0-9]+).*,\1,p'`
        if [ x"$displaynum" != x"" ]; then
                export DISPLAY=":$displaynum"
                getXuser
        fi
}
